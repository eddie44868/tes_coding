import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Putra Editia"),
            currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/avatar.png')),
            accountEmail: Text("putra12373349@gmail.com"),
          ),
          // ignore: prefer_const_constructors
          DrawerListTile(
            iconData: Icons.home,
            title: "Beranda",
          ),
          const Divider(
            color: Colors.white,
          ),
          const DrawerListTile(
            iconData: Icons.monitor,
            title: "Ujian",
          ),
          Divider(
            color: Colors.white,
          ),
          DrawerListTile(
            iconData: Icons.notifications_active_outlined,
            title: "Notifikasi",
            onTilePressed: () {},
          ),
          Divider(
            color: Colors.white,
          ),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Perpustakaan Digital",
            onTilePressed: () {},
          ),
          Divider(
            color: Colors.white,
          ),
          DrawerListTile(
            iconData: Icons.bookmark_border,
            title: "Materi 4",
            onTilePressed: () {},
          ),
          Divider(
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16, color: Colors.white),
      ),
    );
  }
}
