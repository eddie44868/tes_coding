import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tes_app/api_list.dart';
import 'package:tes_app/drawer.dart';
import 'package:http/http.dart' as http;


class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.indigo[900],
        ),
        child: DrawerScreen(),
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 35),
            const Text(
              "Materi Pembelajaran",
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 40),
            Icon(Icons.notifications_active_outlined, color: Colors.white,),
          ],
        ),
      ),
      body: ListView(
        children: [
          Image(
            fit: BoxFit.fill,
            image: AssetImage('assets/top_home.png')),
            Container(
              height: 300,
              child: ApiList(),
              ),
        ],
      ),
    );
  }
}
