import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tes_app/model/model.dart';

class ApiList extends StatefulWidget {
  @override
  _ApiListState createState() => _ApiListState();
}

class _ApiListState extends State<ApiList> {
    List<model> listData = [];
  var loading = false;
  getModels() async {
    setState(() {
      loading = true;
    });
    var apiURL = "http://tes-mobile.landa.id/index.php";
    var apiResult = await http.get(Uri.parse(apiURL));
    print(apiResult.body);
    //var jsonObject = json.decode(apiResult.body);
    if (apiResult.statusCode == 200) {
      var data = jsonDecode(apiResult.body);
      setState(() {
          listData.add(model.fromJson(data));
        loading = false;
      });
    }
  }

   @override
  Widget build(BuildContext context) {
    return Container(
        child: loading
        ? const Center(child: CircularProgressIndicator())
        : ListView.builder(
          itemCount: listData.length,
          itemBuilder: (context, i) {
            final x = listData[i];
            return Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(x.nama[i].toString()),
                ],
              ),
            );
          }
          ),
    );
  }
}
