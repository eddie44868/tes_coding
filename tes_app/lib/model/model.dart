class model {
  String nama;
  detail details;
  model({this.nama, this.details});

  factory model.fromJson(Map<String, dynamic> json) {
    return model(
      nama: json['nama'].toString(),
      details: detail.fromJson(json['detail']),
    );
  }
}

class detail {
  String url;

  detail({this.url});
    factory detail.fromJson(Map<String, dynamic> json) {
    // ignore: unnecessary_new
    return new detail(
     url: json['youtube_url'].toString(),
    );
  }
}